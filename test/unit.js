let universe      = (new Function('return this;')).call();
let chalk         = require('chalk');
let getCallerFile = require('get-caller-file');

let running = false;
let pending = [];
let timer   = undefined;

universe.deepEqual = function(a, b) {
  if ( (typeof a) !== (typeof b) ) return false;
  if ( 'function' === typeof a ) return a.toString() === b.toString();
  if ( a === b || a.valueOf() === b.valueOf() ) return true;
  if (!(a instanceof Object)) return false;
  let ka = Object.keys(a);
  if (ka.length !== Object.keys(b).length) {
    return false;
  }
  return Object.keys(b).reduce(function(r,k) {
    if (!r) return false;
    if (!~ka.indexOf(k)) return false;
    return universe.deepEqual(a[k],b[k]);
  }, true);
};

universe.test = function( name, f ) {
  if ( 'function' === typeof name ) {
    f    = name;
    name = getCallerFile();
  }
  if ( 'function' !== typeof f ) {
    return;
  }
  pending.push({ name, f });
  if (running) return;
  if ('undefined' !== typeof timer) {
    clearTimeout(timer);
  }
  timer = setTimeout(function run(test) {

    // Looping logic
    running = true;
    if (!test) {
      if (!pending.length) return running = false;
      return run(pending.shift());
    }

    // Notify we're running
    process.stdout.write(chalk.black.bgYellow(" TEST "));
    process.stdout.write(chalk.yellow('') + ' ' + test.name);

    // Finishing callback
    let finished = false;
    function done( err, message ) {
      if (finished) return;
      finished = true;
      process.stdout.write('\r');
      if (err) {
        process.stdout.write(chalk.black.bgRed(" FAIL "));
        process.stdout.write(chalk.red('') + ' ' + (message||test.name));
        process.stdout.write('\n');
        console.log(err);
        process.exit(1);
      } else {
        process.stdout.write(chalk.black.bgGreen(" PASS "));
        process.stdout.write(chalk.green('') + ' ' + (message||test.name));
        process.stdout.write('\n');
      }
      run();
    }

    // Start the test
    try {
      test.f(done)
    } catch(e) {
      done(e);
    }
  }, 200);
};
