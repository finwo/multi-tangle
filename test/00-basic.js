
// Basic in-memory adapter
let adapter = {
  data: {},
  get: function( key, callback ) {
    callback(undefined,adapter.data[key]);
  },
  put: function( key, value, callback ) {
    adapter.data[key] = value;
    callback();
  }
}

// Where we'll initialize the database
let db;

// Initial setup
test('Basic setup', function(done) {
  db = require('../index')({
    storage: [adapter],
  });
  done();
});

// Test kv.put wrapper
test('KV.put wrapper', function(done) {
  db._put('greeting', 'hello');
  db._put('subject', 'world');
  if (!deepEqual(adapter.data,{greeting:'"hello"',subject:'"world"'})) {
    throw new Error("Data was not stored");
  }
  done();
});

// Test kv.get wrapper
test('KV.get wrapper', function(done) {
  let received;
  db._get('greeting', function(err, data) {received = data;});
  if (received !== 'hello') throw new Error("Invalid data received");
  db._get('subject', function(err, data) {received = data;});
  if (received !== 'world') throw new Error("Invalid data received");
  done();
});
