const http   = require('http');
const Tangle = require('../index');
const httpd  = http.createServer();

// Create node vars
let alice,bob,charlie;

// Network topology:
//   alice <--> bob <--> charlie

// Initialize alice
alice = Tangle({
  network: [{send:function(msg) {
    if (!bob) return;
    bob._hear(msg);
  }}]
});

// Initialize bob
bob = Tangle({
  super  : true,
  network: [{send:function(msg) {
    if (!alice) return;
    if (!charlie) return;
    alice._hear(msg);
    charlie._hear(msg);
  }}]
});

// Initialize charlie
charlie = Tangle({
  network: [{send:function(msg) {
    if (!bob) return;
    bob._hear(msg);
  }}]
});

// Write some data
alice.put('kittens', { commonName: 'fluffy' });
charlie.get('kittens', function() {
  console.log('GOT', arguments);
});

console.log(alice);
