// SRC: https://npmjs.com/package/protocols
function protocols( input, first ) {
  if (first === true) first = 0;
  let index  = input.indexOf("://"),
      splits = input.substring(0, index).split('+').filter(Boolean);
  if (typeof first === 'number') {
    return splits[first];
  }
  return splits;
}

module.exports = {
  init: function() {
    let ctx = this;

    // Fetch the ws library
    let WS = ctx.WebSocket || require('cws');
    if (!WS) return;

    // Allow disabling the adapter
    if (ctx.ws === false) return;

    // Fetch how to handle http requests
    let httpHandler = 'function' === typeof ctx.http ? ctx.http : function( req, res ) {
      res.end('Nothing to see here...');
    };

    // Ensure WS is an object
    if ( 'object' !== typeof ctx.ws ) {
      ctx.ws = {};
    }

    // Start a server if requested
    if (ctx.ws.server) {
      let wss = ctx.ws.wss = new WebSocket.Server(ctx.ws);
      wss.on('connection', function connection(socket) {
        let peer = {
          queue: [],
          send : function(msg) {
            if (socket.readyState > 1) return;
            if (socket.readyState < 1) return peer.queue.push(msg);
            peer.queue.push(msg);
            while(peer.queue.length) socket.send(peer.queue.shift());
          }
        };
        socket.on('message', function message(msg) {
          ctx._hear(msg);
        });
        ctx.network.push(peer);
      });
    }

    // Start websocke client(s)
    ctx.network = ctx.network.map(function(peer) {
      if ('string' !== typeof peer) return peer;
      let url   = peer;
      let prots = protocols(url);
      if (!(~prots.indexOf('ws')||~prots.indexOf('wss'))) return peer;
      function reconnect(timeout) {
        timeout = timeout || 100;
        if ('object' === typeof peer) {
          if (peer.ignore) return;
          peer.ignore = true;
          try {
            (peer.ws.terminate||peer.ws.close)();
          } catch(e) {}
        } else {
          peer = {};
        }
        peer = {
          ws   : new WebSocket(url),
          queue: peer.queue || [],
          send : function(msg) {
            if ( peer.ignore ) return;
            if (msg) peer.queue.push(msg);
            if ( peer.ws.readyState !== 1 ) return;
            while(peer.queue.length) peer.ws.send(peer.queue.shift());
          },
        };
        peer.ws.onopen    = function() { peer.send(); };
        peer.ws.onclose   = function() { setTimeout(reconnect,timeout); };
        peer.ws.onmessage = function(msg) {
          ctx._hear(msg);
        };
        peer.ws.onerror = function(err) {
          if (!err) return;
          if (err.code === 'ECONNREFUSED')
            setTimeout(reconnect.bind(null,timeout*2),timeout);
        }
      }
      reconnect();
      return peer;
    });
  };
  // TODO: client init
  // TODO: server init
};
