let universe = (new Function('return this;')).call(),
    noop     = function(){};

function randStr( length, alphabet ) {
  alphabet   = alphabet || '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
  length     = length   || 48;
  let result = '';
  while( result.length < length ) {
    result += alphabet.charAt(Math.floor(Math.random()*alphabet.length));
  }
  return result;
}

function hashCode( subj ) {
  if ('string' !== typeof subj) return 0;
  let a = 1,
      b = 0;
      m = 65521,
      i = 0,
      l = subj.length;
  while( i<l ) {
    a = ( a + subj.charCodeAt(i) ) % m;
    b = ( a + b                  ) % m;
    i++;
  }
  return ( b<<16 ) + a;
}

function simpleMerge( parents, data ) {
  if ( 'object' !== typeof data ) return data;
  if ( !data ) return data;
  let out = {};
  parents.forEach(function(parent) {
    Object.assign(out, parent);
  });
  return out;
};

// Main constructor
function MultiTangle( options ) {
  if ( this === universe ) return new MultiTangle(options);
  let ctx = this;

  // Load config
  Object.assign(ctx, {
    storage: [],          // Local storage adapters
    network: [],          // Network adapters (peers, not libs)
    super  : false,       // Whether this is a super peer
    merge  : simpleMerge, // How to merge data together
    dedup  : [],          // Network deduplication list
    seq    : 0,           // Current sequence for dedup smashing
    root   : ctx,
    data   : {},
  }, options);

  // Generate peer id
  this.id = this.id || randStr();

  // Initialize adapters
  ctx.storage.forEach(function(adapter) {
    if ( 'object'   !== typeof adapter      ) return;
    if ( 'function' !== typeof adapter.init ) return;
    adapter.init.call( ctx );
  });
  ctx.network.forEach(function(adapter) {
    if ( 'object'   !== typeof adapter      ) return;
    if ( 'function' !== typeof adapter.init ) return;
    adapter.init.call( ctx );
  });

  // Broadcast our presence to the network
  try {
    this._say({ op: 'join', tx: this.id });
  } catch(e) {
    console.log(e);
  }

  return this;
}

// Sequence geneerator
// For dedup busting
MultiTangle.prototype._seq = function() {
  return (this.seq = (this.seq + 1) % 8192);
};

// Fetch from local storage
MultiTangle.prototype._get = function( key, callback ) {
  let queue = this.storage.slice();
  let ctx   = this;
  callback  = callback || noop;
  (function next(err, data) {
    if (err) return callback(err);
    if ('undefined' !== typeof data) return callback(undefined,JSON.parse(data));
    if (!queue.length) return callback(undefined,undefined);
    let adapter = queue.shift();
    if ('function' !== typeof adapter.get) return next();
    adapter.get.call(ctx, key, next);
  })();
};

// Write into local storage
MultiTangle.prototype._put = function( key, value, callback ) {
  let queue  = this.storage.slice();
  let errors = [];
  let ctx    = this;
  let data   = JSON.stringify(value);
  callback   = callback || noop;
  (function next(err) {
    if (err) errors.push(err);
    if (!queue.length) return callback(errors.lenth ? errors : undefined);
    let adapter = queue.shift();
    if ('function' !== typeof adapter.put) return next();
    adapter.put.call(ctx, key, data, next);
  })();
};

// Transmit into the network
MultiTangle.prototype._say = function( message, callback ) {
  let queue = this.network.slice();
  let ctx   = this;
  let str   = 'string' === typeof message ? message : JSON.stringify(message);
  callback  = callback || noop;
  (function next() {
    if (!queue.length) return callback();
    let adapter = queue.shift();
    if ('object'   !== typeof adapter     ) return next();
    if ('function' !== typeof adapter.send) return next();
    adapter.send.call(ctx, str);
    next();
  })();
};

// Hear from the network
MultiTangle.prototype._hear = function( message ) {
  if ('string' !== typeof message) return;
  let ctx = this;

  // Message deduplication
  let h = hashCode(message);
  if (~ctx.dedup.indexOf(h)) return;
  ctx.dedup.push(h);
  if ( ctx.dedup.length > 4096 ) ctx.dedup.shift();

  // Decode the message
  let data;
  try {
    data = JSON.parse(message);
  } catch(e) {return;}
  if (!('op' in data)) return;

  // Handle request
  switch(data.op) {
    case 'join':
      // Maybe useful in the future
      ctx._say(message);
      break;
    case 'get':
      if ('string' !== typeof data.id) return;
      ctx._get(data.id, function(err, fetched) {
        if ('undefined' === typeof fetched) return;
        ctx._hear(JSON.stringify(Object.assign({}, fetched, {
          op  : 'data',
          tx  : ctx.id,
          seq : ctx._seq(),
        })))
      });
      ctx._say(message);
      break;
    case 'head':
      console.log('GET HEAD');
      break;
    case 'data':
      // Sanity checks
      if (!('id'      in data)) return;
      if (!('data'    in data)) return;
      if (!('parents' in data)) return;
      if (!('tangle'  in data)) return;
      // Fetch current heads
      ctx._get(data.tangle, function(err, fetched) {
        fetched = fetched || [];
        // TODO: fetch parents etc
      })
      c
      ctx._say(message);
      console.log('HANDLE PUT');
      break;
    default:
      throw new Error('Unknown op: ' + data.op);
  }
};

// Write data
// Only subscribed nodes & super peers can write to spme data
MultiTangle.prototype.put = function( tangle, data, callback ) {
  if ('string' !== typeof tangle) return;
  let ctx = this;



  // // Fetch known heads
  // ctx._get(tangle, function( openHeads ) {

    // // Build message
    // let message = {
    //   op      : 'data',
    //   tx      : ctx.id,
    //   seq     : ctx._seq(),
    //   id      : randStr(),
    //   data    : data,
    //   parents : openHeads,
    //   tangle  : tangle,
    // };

    // // Transmit it
    // // On our input, so we save it as well
    // // This saves us from having to write the code twice
    // ctx._hear(JSON.stringify(message));
  // });

  // Return our context
  return ctx;
};

MultiTangle.prototype.get = function( tangle, callback ) {
  if ('string' !== typeof tangle) return;
  let ctx = this;

  //
  return ctx;
};



// Output our produce
module.exports = MultiTangle;
